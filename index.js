require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');

// create express app
const app = express();

// create redis
const redis = require('redis');
const url = require('url');
const redisURL = url.parse(process.env.REDIS_URL);
const client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
client.auth(redisURL.auth.split(":")[1]);

// connecting to redis
client.on('connect', function() {
    console.log('Redis client connected');
});
client.on("error", function (err) {
    console.log("Error " + err);
});

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// define a simple route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to Wirendy application. Please see the documentation.txt"});
});

// Require Users routes
require('./app/routes/user.routes.js')(app);

const port = process.env.PORT || 3000;
// listen for requests
app.listen(port, () => {
    console.log("Server is listening on port 3000");
});