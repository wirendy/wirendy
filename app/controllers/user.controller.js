const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const url = require('url');
const redisURL = url.parse(process.env.REDIS_URL);
const client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
client.auth(redisURL.auth.split(":")[1]);

// Get Token
exports.getToken = (req, res) => {
    // Mock user
    const user = {
        username: 'wirendy',
        email: 'wirendy@gmail.com'
    }

    jwt.sign({user}, 'secretkey', { expiresIn: '600s' }, (err, token) => {
        res.json({
            token
        });
    });
};

// Create and Save a new User
exports.create = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Validate request
            if(!req.body.userName) {
                return res.status(400).send({
                    message: "Username can not be empty"
                });
            }
            if(!req.body.accountNumber) {
                return res.status(400).send({
                    message: "User account number can not be empty"
                });
            }
            if(!req.body.emailAddress) {
                return res.status(400).send({
                    message: "Email address can not be empty"
                });
            }
            if(!req.body.identityNumber) {
                return res.status(400).send({
                    message: "User identity number can not be empty"
                });
            }

            // Create a User
            const user = new User({
                userName: req.body.userName, 
                accountNumber: req.body.accountNumber,
                emailAddress: req.body.emailAddress,
                identityNumber: req.body.identityNumber
            });

            // Save User in the database
            user.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the User."
                });
            });
        }
    });
};

// Retrieve and return all users from the database.
exports.findAll = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Search All User
            User.find()
            .then(users => {
                res.send(users);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving users."
                });
            });
        }
    });
};

// Find a single user with a userId
exports.findOneUserId = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Search User by UserID
            User.findById(req.params.userId)
            .then(user => {
                if(!user) {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });            
                }
                // Cache Data userId
                client.set(req.params.userId, user);
                res.send(user);
            }).catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });                
                }
                return res.status(500).send({
                    message: "Error retrieving user with id " + req.params.userId
                });
            });
        }
    });
};

// Find a single user with a accountNumber
exports.findOneAccountNumber = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Search User by AccountNumber
            User.findOne({ accountNumber: req.params.accountNumber })
            .then(user => {
                if(!user) {
                    return res.status(404).send({
                        message: "User not found with account number " + req.params.accountNumber
                    });            
                }
                // Cache Data accountNumber
                client.set(req.params.accountNumber, user);
                res.send(user);
            }).catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with account number " + req.params.accountNumber
                    });                
                }
                return res.status(500).send({
                    message: "Error retrieving user with account number " + req.params.accountNumber
                });
            });
        }
    });
};

// Find a single user with a identityNumber
exports.findOneIdentityNumber = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Search User by identityNumber
            User.findOne({ identityNumber: req.params.identityNumber })
            .then(user => {
                if(!user) {
                    return res.status(404).send({
                        message: "User not found with identity number " + req.params.identityNumber
                    });            
                }
                // Cache Data identityNumber
                client.set(req.params.identityNumber, user);
                res.send(user);
            }).catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with identity number " + req.params.identityNumber
                    });                
                }
                return res.status(500).send({
                    message: "Error retrieving user with identity number " + req.params.identityNumber
                });
            });
        }
    });
};

// Update a user identified by the userId in the request
exports.update = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Validate request
            if(!req.body.userName) {
                return res.status(400).send({
                    message: "Username can not be empty"
                });
            }
            if(!req.body.accountNumber) {
                return res.status(400).send({
                    message: "User account number can not be empty"
                });
            }
            if(!req.body.emailAddress) {
                return res.status(400).send({
                    message: "Email address can not be empty"
                });
            }
            if(!req.body.identityNumber) {
                return res.status(400).send({
                    message: "User identity number can not be empty"
                });
            }

            // Find user and update it with the request body
            User.findByIdAndUpdate(req.params.userId, {
                userName: req.body.userName, 
                accountNumber: req.body.accountNumber,
                emailAddress: req.body.emailAddress,
                identityNumber: req.body.identityNumber
            }, {new: true})
            .then(user => {
                if(!user) {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });
                }
                // Update Cache Data on Redis by User ID
                client.set(req.params.userId, user);
                res.send(user);
            }).catch(err => {
                if(err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });                
                }
                return res.status(500).send({
                    message: "Error updating user with id " + req.params.userId
                });
            });
        }
    });
};

// Delete a user with the specified userId in the request
exports.delete = (req, res) => {
    // Verify Token
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            // Forbidden
            return res.status(403).send({
                message: "Forbidden"
            });
        } else {
            // Delete User From DB
            User.findByIdAndRemove(req.params.userId)
            .then(user => {
                if(!user) {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });
                }
                // Delete Cache UserId on Redis
                client.del(req.params.userId);
                res.send({message: "User deleted successfully!"});
            }).catch(err => {
                if(err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.userId
                    });                
                }
                return res.status(500).send({
                    message: "Could not delete user with id " + req.params.userId
                });
            });
        }
    });
};