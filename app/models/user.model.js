const mongoose = require('mongoose');
const validator = require('validator');

const UserSchema = mongoose.Schema({
    userName: {
        type: String,
        required: true,
        unique: true
    },
    accountNumber: {
        type: Number,
        required: true,
        minlength: 6
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true,
        // Validasi Email
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid!')
            }
        }
    },
    identityNumber: {
        type: Number,
        required: true,
        minlength: 6
    },
}, {
    timestamps: true
});
// Indexing Id
UserSchema.index({ _id: 1 }, { sparse: true });

module.exports = mongoose.model('User', UserSchema);