module.exports = (app) => {
    const users = require('../controllers/user.controller.js');

    // Get Token
    app.post('/users/token', users.getToken);

    // Create a new User
    app.post('/users', verifyToken, users.create);

    // Retrieve all User
    app.get('/users', verifyToken, users.findAll);

    // Retrieve a single User with userId
    app.get('/users/get-user-by-id/:userId', verifyAndCacheUserId, users.findOneUserId);

    // Retrieve a single User with accountNumber
    app.get('/users/get-user-by-account-number/:accountNumber', verifyAndCacheAccountNumber, users.findOneAccountNumber);

    // Retrieve a single User with identityNumber
    app.get('/users/get-user-by-identity-number/:identityNumber', verifyAndCacheIdentityNumber, users.findOneIdentityNumber);

    // Update a User with userId
    app.put('/users/:userId', verifyToken, users.update);

    // Delete a User with userId
    app.delete('/users/:userId', verifyToken, users.delete);
}

const User = require('../models/user.model.js');
// create redis
const redis = require('redis');
const url = require('url');
const redisURL = url.parse(process.env.REDIS_URL);
const client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
client.auth(redisURL.auth.split(":")[1]);

// FORMAT OF TOKEN
// Authorization: Bearer <access_token>

// Verify Token
function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        next();
    } else {
        // Forbidden
        return res.status(403).send({
            message: "Forbidden"
        });
    }
}

// Use Cache UserId
function verifyAndCacheUserId(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        const userId = req.params.userId;
        client.get(userId, function (err, data) {
            if (err) throw err;

            if (data != null) {
                res.send(data);
            } else {
                next();
            }
        });
    } else {
        // Forbidden
        return res.status(403).send({
            message: "Forbidden"
        });
    }
}

// Use Cache AccountNumber
function verifyAndCacheAccountNumber(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        const accountNumber = req.params.accountNumber;
        client.get(accountNumber, function (err, data) {
            if (err) throw err;

            if (data != null) {
                res.send(data);
            } else {
                next();
            }
        });
    } else {
        // Forbidden
        return res.status(403).send({
            message: "Forbidden"
        });
    }
}

// Use Cache IdentityNumber
function verifyAndCacheIdentityNumber(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        const identityNumber = req.params.identityNumber;
        client.get(identityNumber, function (err, data) {
            if (err) throw err;

            if (data != null) {
                res.send(data);
            } else {
                next();
            }
        });
    } else {
        // Forbidden
        return res.status(403).send({
            message: "Forbidden"
        });
    }
}